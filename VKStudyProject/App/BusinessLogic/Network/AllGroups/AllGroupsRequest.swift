import Foundation
import Alamofire

struct AllGroupsRequest: RequestRouter {

    let apiVersion: String
    var baseUrl: URL
    var searchText: String

    let accessToken: String = CurrentUser().token() ?? ""

    var method: HTTPMethod = .get
    var path: String = "groups.search"

    var parameters: Parameters? {
        return [
            "access_token": accessToken,
            "q": searchText,
            "extended": "1",
            "sort": "3",
            "count": "30",
            "v": apiVersion
        ]
    }
}
