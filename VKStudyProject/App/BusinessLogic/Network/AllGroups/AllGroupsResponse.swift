import Foundation
import UIKit

struct AllGroupsResponseWrapped: Decodable {
    let response: AllGroupsResponse
}

struct AllGroupsResponse: Decodable {
    let count: Int
    let items: [AllGroup]
}

struct AllGroup: Decodable {
    let groupId: Int
    let nameGroup: String
    let photoGroup: String

    enum CodingKeys: String, CodingKey {
        case groupId = "id"
        case nameGroup = "name"
        case photoGroup = "photo_200"
    }
}
