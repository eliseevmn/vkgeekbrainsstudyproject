import Foundation
import Alamofire

protocol AllGroupsService {
    func getAllGroups(searchText: String, completion: @escaping (AllGroupsResponseWrapped?) -> Void)
}

class AllGroupsServiceImpl: AllGroupsService {
    private var baseUrl: URL
    private let networkService: NetworkService

    init(baseUrl: URL, networkService: NetworkService) {
        self.baseUrl = baseUrl
        self.networkService = networkService
    }

    func getAllGroups(searchText: String, completion: @escaping (AllGroupsResponseWrapped?) -> Void) {
        let requestModel = AllGroupsRequest(
            apiVersion: Configuration().apiVersion,
            baseUrl: baseUrl,
            searchText: searchText)
        networkService.request(requestModel) { (response: AllGroupsResponseWrapped?) in
            completion(response)
        }
    }
}
