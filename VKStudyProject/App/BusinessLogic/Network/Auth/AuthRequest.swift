import Foundation
import Alamofire

struct AuthRequest: RequestRouter {
    var baseUrl: URL
    var method: HTTPMethod = .get
    var path: String = "authorize"
    var parameters: Parameters? {
        return [
            "client_id": Configuration().idApi,
            "display": "mobile",
            "redirect_uri": "https://oauth.vk.com/blank.html",
            "scope": "270342", // 262144 + 2 + 4 + 8192
            "response_type": "token",
            "v": Configuration().apiVersion
        ]
    }
}
