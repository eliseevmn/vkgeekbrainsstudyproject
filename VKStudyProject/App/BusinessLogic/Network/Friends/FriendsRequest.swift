import Foundation
import Alamofire

struct FriendsRequest: RequestRouter {

    let apiVersion: String
    var baseUrl: URL

    let accessToken: String = CurrentUser().token() ?? ""
    let userId: Int = CurrentUser().id() ?? 0

    var method: HTTPMethod = .get
    var path: String = "friends.get"
    var parameters: Parameters? {
        return [
            "user_id": userId,
            "access_token": accessToken,
            "order": "name",
            "fields": "nickname, photo_100",
            "v": apiVersion
        ]
    }
}
