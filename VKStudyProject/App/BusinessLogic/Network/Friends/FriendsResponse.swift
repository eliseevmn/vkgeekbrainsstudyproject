import Foundation

struct ProfilesResponseWrapped: Codable, Hashable {
    let response: ProfilesResponse
}

struct ProfilesResponse: Codable, Hashable {
    let count: Int
    let items: [Profile]
}

struct Profile: Codable, Hashable {
    let userId: Int
    let firstName: String
    let lastName: String
    let photUserUrl: String

    enum CodingKeys: String, CodingKey {
        case userId = "id"
        case firstName = "first_name"
        case lastName = "last_name"
        case photUserUrl = "photo_100"
    }
}
