import Foundation
import Alamofire

protocol FriendsService {
    func getFriends(completion: @escaping (ProfilesResponseWrapped?) -> Void)
}

class FriendsServiceImpl: FriendsService {
    private var baseUrl: URL
    private let networkService: NetworkService

    init(baseUrl: URL, networkService: NetworkService) {
        self.baseUrl = baseUrl
        self.networkService = networkService
    }

    func getFriends(completion: @escaping (ProfilesResponseWrapped?) -> Void) {
        let requestModel = FriendsRequest(apiVersion: Configuration().apiVersion, baseUrl: baseUrl)
        networkService.request(requestModel) { (response: ProfilesResponseWrapped?) in
            completion(response)
        }
    }
}
