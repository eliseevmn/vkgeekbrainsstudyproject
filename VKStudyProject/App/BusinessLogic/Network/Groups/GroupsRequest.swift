import Foundation
import Alamofire

struct GroupsRequest: RequestRouter {

    let apiVersion: String
    var baseUrl: URL

    let accessToken: String = CurrentUser().token() ?? ""
    let userId: Int = CurrentUser().id() ?? 0

    var method: HTTPMethod = .get
    var path: String = "groups.get"
    var parameters: Parameters? {
        return [
            "user_id": userId,
            "access_token": accessToken,
            "extended": 1,
            "fields": "members_count",
            "v": apiVersion
        ]
    }
}
