import Foundation
import UIKit

struct GroupsResponseWrapped: Decodable {
    let response: GroupsResponse
}

struct GroupsResponse: Decodable {
    let count: Int
    let items: [Group]
}

struct Group: Decodable, Hashable {
    let groupId: Int
    let nameGroup: String
    let membersCountGroup: Int
    let photoGroupUrl: String?
    let description: String?

    enum CodingKeys: String, CodingKey {
        case groupId = "id"
        case nameGroup = "name"
        case membersCountGroup = "members_count"
        case photoGroupUrl = "photo_200"
        case description
    }
}
