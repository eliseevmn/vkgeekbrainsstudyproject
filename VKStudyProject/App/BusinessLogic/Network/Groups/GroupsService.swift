import Foundation
import Alamofire

protocol GroupsService {
    func getMyGroups(completion: @escaping (GroupsResponseWrapped?) -> Void)
}

class GroupsServiceImpl: GroupsService {
    private var baseUrl: URL
    private let networkService: NetworkService

    init(baseUrl: URL, networkService: NetworkService) {
        self.baseUrl = baseUrl
        self.networkService = networkService
    }

    func getMyGroups(completion: @escaping (GroupsResponseWrapped?) -> Void) {
        let requestModel = GroupsRequest(apiVersion: Configuration().apiVersion, baseUrl: baseUrl)
        networkService.request(requestModel) { (response: GroupsResponseWrapped?) in
            completion(response)
        }
    }
}
