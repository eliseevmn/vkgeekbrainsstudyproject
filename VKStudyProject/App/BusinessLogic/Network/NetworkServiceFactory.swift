import UIKit
import Alamofire

// Фабрика по созданию сетевых запросов
class NetworkServiceFactory {

    static let shared: NetworkServiceFactory = NetworkServiceFactory()

    private(set) lazy var commonSessionManager: Session = {
        let configuration = URLSessionConfiguration.default
        configuration.httpShouldSetCookies = false
        let manager = Session(configuration: configuration)
        return manager
    }()

    private let configuration = Configuration()
    private let errorParser = ErrorParser()

    private(set) lazy var networkService: NetworkService = NetworkServiceImpl(
        errorParser: errorParser,
        sessionManager: commonSessionManager)

    private init() {}

    func makeAuthRequest() -> URLRequest? {
        let request = try? AuthRequest(baseUrl: URL(string: configuration.oauthUrl)!).asURLRequest()
        return request
    }

    func makeProfilRequesFactory() -> ProfileService {
        return ProfileServiceImpl(baseUrl: URL(string: configuration.baseUrl)!, networkService: networkService)
    }

    func makePhotoRequestFactory() -> PhotoService {
        return PhotoServiceImpl(baseUrl: URL(string: configuration.baseUrl)!, networkService: networkService)
    }

    func makeFriendsListRequestFactory() -> FriendsService {
        return FriendsServiceImpl(baseUrl: URL(string: configuration.baseUrl)!, networkService: networkService)
    }

    func makeGroupsRequestFactory() -> GroupsService {
        return GroupsServiceImpl(baseUrl: URL(string: configuration.baseUrl)!, networkService: networkService)
    }

    func makeAllGroupsRequestFactory() -> AllGroupsService {
        return AllGroupsServiceImpl(baseUrl: URL(string: configuration.baseUrl)!, networkService: networkService)
    }

    func makeNewsRequestFactory() -> NewsService {
        return NewsServiceImpl(baseUrl: URL(string: configuration.baseUrl)!, networkService: networkService)
    }
}
