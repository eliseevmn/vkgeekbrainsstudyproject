import Foundation
import Alamofire

struct NewsRequest: RequestRouter {

    let apiVersion: String
    var baseUrl: URL
    var startFrom: String

    let accessToken: String = CurrentUser().token() ?? ""

    var method: HTTPMethod = .get
    var path: String = "newsfeed.get"

    var parameters: Parameters? {
        return [
            "access_token": accessToken,
            "filters": "post",
            "start_from": startFrom,
            "count": "10",
            "v": apiVersion
        ]
    }
}
