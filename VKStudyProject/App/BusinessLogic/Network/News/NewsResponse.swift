import UIKit

struct NewsFeedResponseWrapped: Decodable {
    let response: NewsFeedResponse
}

struct NewsFeedResponse: Decodable {
    var items: [NewsFeedModel]
    var profiles: [ProfileNews]
    var groups: [GroupNews]
    var nextFrom: String?

    enum CodingKeys: String, CodingKey {
        case items, profiles, groups
        case nextFrom = "next_from"
    }
}

struct NewsFeedModel: Decodable {
    let sourceId: Int
    let postId: Int
    let text: String?
    let date: Double
    let comments: CountableItem?
    let likes: CountableItem?
    let reposts: CountableItem?
    let views: CountableItem?
    let attachments: [Attachments]?

    enum CodingKeys: String, CodingKey {
        case sourceId = "source_id"
        case postId = "post_id"
        case text, date, comments, likes, reposts, views, attachments
    }
}

struct CountableItem: Decodable {
    let count: Int
}

struct Attachments: Decodable {
    let photo: PhotoFromNews?
}

struct PhotoFromNews: Decodable {
    let sizes: [PhotoSize]

    var height: Int {
        return getPropperSize().height
    }

    var width: Int {
        return getPropperSize().width
    }

    var srcBIG: String {
        return getPropperSize().url
    }

    private func getPropperSize() -> PhotoSize {
        if let sizeX = sizes.first(where: { $0.type == "x"}) {
            return sizeX
        } else if let fallBackSize = sizes.last {
            return fallBackSize
        } else {
            return PhotoSize(type: "wrong image", url: "wrong image", width: 0, height: 0)
        }
    }
}

struct FeedViewModel {

    struct Cell: FeedCellViewModel {
        var iconUrlString: String
        var name: String
        var date: String
        var text: String?
        var likes: String?
        var comments: String?
        var shares: String?
        var views: String?

        var photoAttachements: [FeedCellPhotoAttachementViewModel]
        var sizes: FeedCellSizes
    }

    struct FeedCellPhotoAttachment: FeedCellPhotoAttachementViewModel {
        var photoUrlString: String?
        var width: Int
        var height: Int
    }

    var cell: [Cell]
}

protocol ProfileRepresentable {

    var idNumber: Int { get }
    var name: String { get }
    var photo: String { get }
}

struct ProfileNews: Decodable, ProfileRepresentable {
    let idNumber: Int
    let firstName: String
    let lastName: String
    let photo100: String

    var name: String { return firstName + " " + lastName }
    var photo: String { return photo100 }

    enum CodingKeys: String, CodingKey {
        case idNumber = "id"
        case firstName = "first_name"
        case lastName = "last_name"
        case photo100 = "photo_100"
    }
}

struct GroupNews: Decodable, ProfileRepresentable {
    var idNumber: Int
    var name: String
    var photo100: String

    var photo: String { return photo100 }

    enum CodingKeys: String, CodingKey {
        case idNumber = "id"
        case name
        case photo100 = "photo_100"
    }
}

protocol FeedCellViewModel {
    var iconUrlString: String { get }
    var name: String { get }
    var date: String { get }
    var text: String? { get }
    var likes: String? { get }
    var comments: String? { get }
    var shares: String? { get }
    var views: String? { get }
    var photoAttachements: [FeedCellPhotoAttachementViewModel] { get }
    var sizes: FeedCellSizes { get }
}

protocol FeedCellSizes {
    var postLabelFrame: CGRect { get }
    var attachmentFrame: CGRect { get }
    var bottomViewFrame: CGRect { get }
    var totalHeight: CGFloat { get }
}

protocol FeedCellPhotoAttachementViewModel {
    var photoUrlString: String? { get }
    var width: Int { get }
    var height: Int { get }
}
