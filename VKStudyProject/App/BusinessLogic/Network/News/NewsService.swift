import Foundation
import Alamofire

protocol NewsService {
    func getNews(completion: @escaping (NewsFeedResponseWrapped?) -> Void)
}

class NewsServiceImpl: NewsService {

    private var baseUrl: URL
    private let networkService: NetworkService

    init(baseUrl: URL, networkService: NetworkService) {
        self.baseUrl = baseUrl
        self.networkService = networkService
    }

    func getNews(completion: @escaping (NewsFeedResponseWrapped?) -> Void) {
        let requestModel = NewsRequest(
            apiVersion: Configuration().apiVersion,
            baseUrl: baseUrl,
            startFrom: "")
        networkService.request(requestModel) { (response: NewsFeedResponseWrapped?) in
            completion(response)
        }
    }
//    func getAllGroups(searchText: String, completion: @escaping (AllGroupsResponseWrapped?) -> Void) {
//        let requestModel = AllGroupsRequest(
//            apiVersion: Configuration().apiVersion,
//            baseUrl: baseUrl,
//            searchText: searchText)
//        networkService.request(requestModel) { (response: AllGroupsResponseWrapped?) in
//            completion(response)
//        }
//    }
}
