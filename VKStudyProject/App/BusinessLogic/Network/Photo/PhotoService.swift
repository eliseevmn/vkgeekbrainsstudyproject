import Foundation
import Alamofire

protocol PhotoService {
    func getPhotoUser(completion: @escaping ([PhotoUserItem]) -> Void)
}

class PhotoServiceImpl: PhotoService {
    private var baseUrl: URL
    private let networkService: NetworkService

    init(baseUrl: URL, networkService: NetworkService) {
        self.baseUrl = baseUrl
        self.networkService = networkService
    }

    func getPhotoUser(completion: @escaping ([PhotoUserItem]) -> Void) {
        let requestModel = PhotoUserRequest(apiVersion: Configuration().apiVersion, baseUrl: baseUrl)
        networkService.request(requestModel) { (response: PhotoUserResponse?) in
            completion(response?.response.items ?? [])
        }
    }
}
