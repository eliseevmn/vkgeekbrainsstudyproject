import Foundation
import Alamofire

struct PhotoUserRequest: RequestRouter {

    let apiVersion: String
    var baseUrl: URL

    let accessToken: String = CurrentUser().token() ?? ""
    let userId: Int = CurrentUser().id() ?? 0

    var method: HTTPMethod = .get
    var path: String = "photos.getAll"
    var parameters: Parameters? {
        return [
            "owner_id": userId,
            "access_token": accessToken,
            "extended": 1,
            "v": apiVersion
        ]
    }
}
