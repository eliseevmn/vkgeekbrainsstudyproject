import Foundation

struct PhotoUserResponse: Codable, Hashable {
    let response: PhotoUserCount
}

struct PhotoUserCount: Codable, Hashable {
    let count: Int
    let items: [PhotoUserItem]
}

struct PhotoUserItem: Codable, Hashable {
    let sizes: [PhotoSize]

    var height: Int {
        return getPropperSize().height
    }

    var width: Int {
        return getPropperSize().width
    }

    var srcBig: String {
        return getPropperSize().url
    }

    private func getPropperSize() -> PhotoSize {
        if let sizeX = sizes.first(where: { $0.type == "x" }) {
            return sizeX
        } else if let fallBackSize = sizes.last {
            return fallBackSize
        } else {
            return PhotoSize(type: "wrong image", url: "wrong image", width: 0, height: 0)
        }
    }
}

struct PhotoSize: Codable, Hashable {
    let type: String
    let url: String
    let width, height: Int
}
