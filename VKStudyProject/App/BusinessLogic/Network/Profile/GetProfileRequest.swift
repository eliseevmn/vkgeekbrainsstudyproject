import Foundation
import Alamofire

struct GetProfileRequest: RequestRouter {

    let apiVersion: String
    var baseUrl: URL

    let accessToken: String = CurrentUser().token() ?? ""
    var method: HTTPMethod = .get
    var path: String = "account.getProfileInfo"
    var parameters: Parameters? {
        return [
            "access_token": accessToken,
            "v": apiVersion
        ]
    }
}
