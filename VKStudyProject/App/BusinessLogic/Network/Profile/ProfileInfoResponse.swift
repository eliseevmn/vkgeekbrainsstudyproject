import Foundation

struct ProfileResponse: Codable, Hashable {
    let response: ProfileInfoResponse
}

struct ProfileInfoResponse: Codable, Hashable {
    let firstName: String
    let lastName: String
    let birthdayDate: String
    let homeTown: String

    enum CodingKeys: String, CodingKey {
        case firstName = "first_name"
        case lastName = "last_name"
        case birthdayDate = "bdate"
        case homeTown = "home_town"
    }
}
