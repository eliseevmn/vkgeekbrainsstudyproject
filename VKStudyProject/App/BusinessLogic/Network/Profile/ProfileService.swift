import Foundation
import Alamofire

protocol ProfileService {
    func getProfileInfo(completion: @escaping (ProfileResponse?) -> Void)
}

class ProfileServiceImpl: ProfileService {

    private var baseUrl: URL
    private let networkService: NetworkService

    init(baseUrl: URL, networkService: NetworkService) {
        self.baseUrl = baseUrl
        self.networkService = networkService
    }

    func getProfileInfo(completion: @escaping (ProfileResponse?) -> Void) {
        let requestModel = GetProfileRequest(apiVersion: Configuration().apiVersion, baseUrl: baseUrl)
        networkService.request(requestModel) { (response: ProfileResponse?) in
            completion(response)
        }
    }
}
