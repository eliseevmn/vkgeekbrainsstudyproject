import Foundation

struct Configuration {
    let oauthUrl: String = "https://oauth.vk.com"
    let baseUrl: String = "https://api.vk.com/method"
    let idApi: String = "7313489"
    let apiVersion: String = "5.103"
}
