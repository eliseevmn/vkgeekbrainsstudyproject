import Foundation

class CurrentUser {

    static let kToken = "Token"
    static let kUserId = "UserId"

    /// Запись данных текущего пользователя
    ///
    /// - Parameters:
    ///   - token: токен для подключения к API VK
    ///   - userId: id пользователя в сервисе VK
    func saveData(token: String, userId: Int) {
        UserDefaults.standard.set(token, forKey: CurrentUser.kToken)
        UserDefaults.standard.set(userId, forKey: CurrentUser.kUserId)
    }

    /// Токен пользователя для подключения к VK
    func token() -> String? {
        return UserDefaults.standard.string(forKey: CurrentUser.kToken)
    }

    /// id пользователя в сервисе VK
    func id() -> Int? {
        return UserDefaults.standard.integer(forKey: CurrentUser.kUserId)
    }
}
