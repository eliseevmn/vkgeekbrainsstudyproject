import UIKit
import WebKit

class AuthorizationController: UIViewController {

    var webview: WKWebView = {
        let view = WKWebView()
        return view
    }()

    private let networkService = NetworkServiceFactory.shared

    override func viewDidLoad() {
        super.viewDidLoad()

        webview.navigationDelegate = self
        configureUI()
        setupVKPage()
    }

    private func configureUI() {
        view.addSubview(webview)
        webview.fillSuperview()
    }

    private func setupVKPage() {
        if let request = networkService.makeAuthRequest() {
            webview.load(request)
        }
    }
}

extension AuthorizationController: WKNavigationDelegate {

    func webView(_ webView: WKWebView,
                 decidePolicyFor navigationResponse: WKNavigationResponse,
                 decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {

        guard let url = navigationResponse.response.url,
            url.path == "/blank.html",
            let fragment = url.fragment  else {
            decisionHandler(.allow)
            return
        }

        let params = fragment
            .components(separatedBy: "&")
            .map { $0.components(separatedBy: "=") }
            .reduce([String: String]()) { result, param in
                var dict = result
                let key = param[0]
                let value = param[1]
                dict[key] = value
                return dict
        }

        guard let token = params["access_token"]  else {
            return
        }

        guard let userId = params["user_id"] else {
            return
        }

        CurrentUser().saveData(token: token, userId: Int(userId) ?? 0)
        decisionHandler(.cancel)

        let controller = MainTabBarController()
        controller.modalPresentationStyle = .fullScreen
        present(controller, animated: true, completion: nil)
    }
}
