import UIKit

class AllGroupsCell: UICollectionViewCell {

    static let reuserId = "AllGroupsCell"

    let iconGroup: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()

    let nameGroupLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont(name: "avenir", size: 20)
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }

    private func setupUI() {
        let horizontalStackView = UIStackView(arrangedSubviews: [
            iconGroup, nameGroupLabel
        ])
        addSubview(horizontalStackView)
        horizontalStackView.axis = .horizontal
        horizontalStackView.spacing = 10
        horizontalStackView.fillSuperview(padding: .init(top: 10,
                                                         left: 16,
                                                         bottom: 10,
                                                         right: 16))
        self.layoutIfNeeded()
        let height = horizontalStackView.frame.height
        iconGroup.widthAnchor.constraint(equalToConstant: height).isActive = true
        iconGroup.layer.cornerRadius = height / 2
        iconGroup.layer.masksToBounds = true
    }

    func setupCell(group: AllGroup?) {
        nameGroupLabel.text = group?.nameGroup
        guard let iconString = group?.photoGroup else {
            iconGroup.image = #imageLiteral(resourceName: "nophoto")
            return
        }
        let url = URL(string: iconString)
        iconGroup.sd_setImage(with: url, completed: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
