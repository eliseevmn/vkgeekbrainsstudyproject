import UIKit

class AllGroupsViewController: UIViewController {

    // MARK: - Properties
    private var isPagination = false
    private var isDonePagination = false
    private var allGroups: [AllGroup] = []
    private var timer: Timer?
    private var searchText: String = ""

    // MARK: - Dependencyry()
    private let allGroupsService = NetworkServiceFactory.shared.makeAllGroupsRequestFactory()

    // MARK: - Outlets
    private let searchController = UISearchController(searchResultsController: nil)
    private var collectionView: UICollectionView!
    private let enterSearchLabel: UILabel = {
        let label = UILabel()
        label.text = "Пожалуйста введите в поле поиска название группы..."
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        return label
    }()

    // MARK: - Controller lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupCollectionViewAndUI()
        setupSearchBar()
    }

    // MARK: - Private functions
    private func setupSearchBar() {
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        searchController.searchBar.placeholder = "Enter group name"
    }

    private func setupCollectionViewAndUI() {
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        collectionView.delegate = self
        collectionView.dataSource = self
        view.addSubview(collectionView)
        collectionView.fillSuperview()

        collectionView.register(AllGroupsCell.self, forCellWithReuseIdentifier: AllGroupsCell.reuserId)

        view.addSubview(enterSearchLabel)
        enterSearchLabel.anchor(top: view.topAnchor,
                                leading: view.leadingAnchor,
                                bottom: nil,
                                trailing: view.trailingAnchor,
                                padding: .init(top: 200, left: 20, bottom: 0, right: 20),
                                size: .init(width: view.frame.width - 40, height: 100))
    }

    private func fetchingData(searchText: String) {
        allGroupsService.getAllGroups(searchText: searchText) { (response) in
            guard let response = response else {
                return
            }
            if response.response.items.count == 0 {
                self.isDonePagination = true
            }

            self.allGroups += response.response.items
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            self.isPagination = false
        }
    }
}

// MARK: - UICollectionViewDelegate, UICollectionViewDataSource
extension AllGroupsViewController: UICollectionViewDelegate, UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        enterSearchLabel.isHidden = allGroups.count != 0
        return allGroups.count
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: AllGroupsCell.reuserId,
            for: indexPath) as? AllGroupsCell else {
                fatalError("Проблема загрузки ячейки")
        }
        let group = allGroups[indexPath.row]
        cell.setupCell(group: group)

        if indexPath.item == allGroups.count - 1 && !isPagination {
            self.isPagination = true
            fetchingData(searchText: searchText)
        }

        return cell
    }
}

// MARK: - UISearchBarDelegate
extension AllGroupsViewController: UISearchBarDelegate {

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { (_) in
            guard !searchText.isEmpty else {
                self.allGroups.removeAll()
                self.collectionView.reloadData()
                return
            }
            self.searchText = searchText
            self.fetchingData(searchText: searchText)
        })
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        allGroups.removeAll()
        collectionView.reloadData()
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension AllGroupsViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: view.frame.width, height: 70)
    }
}
