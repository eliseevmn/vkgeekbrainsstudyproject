import UIKit

class MyGroupsCell: UICollectionViewCell {

    static let reuserId = "MyGroupsCell"

    let iconGroup: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()

    let nameGroupLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont(name: "avenir", size: 20)
        return label
    }()

    let membersCountLabel: UILabel = {
        let label = UILabel()
        label.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        label.font = UIFont(name: "avenir", size: 16)
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupUI()
    }

    private func setupUI() {
        let verticalStackView = UIStackView(arrangedSubviews: [
            nameGroupLabel, membersCountLabel
        ])
        verticalStackView.spacing = 3
        verticalStackView.axis = .vertical

        let horizontalStackView = UIStackView(arrangedSubviews: [
            iconGroup, verticalStackView
        ])
        addSubview(horizontalStackView)
        horizontalStackView.axis = .horizontal
        horizontalStackView.alignment = .center
        horizontalStackView.spacing = 10
        horizontalStackView.fillSuperview(padding: .init(top: 5,
                                                         left: 0,
                                                         bottom: 5,
                                                         right: 0))

        horizontalStackView.layoutIfNeeded()
        let height = horizontalStackView.frame.height
        iconGroup.widthAnchor.constraint(equalToConstant: height).isActive = true
        iconGroup.layer.cornerRadius = height / 2
        iconGroup.layer.masksToBounds = true
    }

    func setupCell(group: Group?) {
        guard let group = group else {
            return
        }
        nameGroupLabel.text = group.nameGroup
        membersCountLabel.text = "Участники: \(group.membersCountGroup)"
        guard let iconString = group.photoGroupUrl else {
            iconGroup.image = #imageLiteral(resourceName: "nophoto")
            return
        }
        let url = URL(string: iconString)
        iconGroup.sd_setImage(with: url, completed: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
