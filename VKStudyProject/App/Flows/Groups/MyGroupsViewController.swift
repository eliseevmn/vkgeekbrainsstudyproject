import UIKit

enum SectionGroup: CaseIterable {
    case group
}

enum SectionGroupType: Hashable {
    case myGroup(Group)
}

class MyGroupsViewController: UIViewController {

    // MARK: - Dependencyry()
    let groupsService = NetworkServiceFactory.shared.makeGroupsRequestFactory()
    var dataSource: UICollectionViewDiffableDataSource<SectionGroup, SectionGroupType>?

    // MARK: - Outlets
    var collectionView: UICollectionView!

    // MARK: - Controller lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupCollectionView()
        createDataSource()
        fetchingData()
        setupNavivagationBarButtons()
    }

    // MARK: - Private functions
    private func setupNavivagationBarButtons() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .add,
            target: self,
            action: #selector(handleAddGroup))
    }

    @objc private func handleAddGroup() {
        let controller = AllGroupsViewController()
        navigationController?.pushViewController(controller, animated: true)
    }

    private func fetchingData() {
        groupsService.getMyGroups { (myGroupsResponse) in
            var snapshot = NSDiffableDataSourceSnapshot<SectionGroup, SectionGroupType>()
            snapshot.appendSections([.group])
            myGroupsResponse?.response.items.forEach({ (group) in
                snapshot.appendItems([.myGroup(group)], toSection: .group)
            })
            self.dataSource?.apply(snapshot)
        }
    }

    private func setupCollectionView() {
        collectionView = UICollectionView(frame: view.bounds, collectionViewLayout: createCompositionalLayout())
        collectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        collectionView.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.9725490196, blue: 0.9921568627, alpha: 1)
        view.addSubview(collectionView)

        collectionView.register(MyGroupsCell.self, forCellWithReuseIdentifier: MyGroupsCell.reuserId)
    }

    // MARK: - Manage the data in UI COmpositional layout
    func createDataSource() {
        dataSource = UICollectionViewDiffableDataSource<SectionGroup, SectionGroupType>(
            collectionView: collectionView,
            cellProvider: { (collectionView, indexPath, groupType) -> UICollectionViewCell? in

            switch groupType {
            case .myGroup(let myGroup):
                let cell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: MyGroupsCell.reuserId,
                    for: indexPath) as? MyGroupsCell
                cell?.setupCell(group: myGroup)
                return cell
            }
        })
    }

    private func createCompositionalLayout() -> UICollectionViewLayout {
        let layout = UICollectionViewCompositionalLayout { (sectionIndex, _) -> NSCollectionLayoutSection? in
            let section = SectionGroup.allCases[sectionIndex]
            switch section {
            case .group:
                return self.createMyGroupSection()
            }
        }
        return layout
    }

    func createMyGroupSection() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                              heightDimension: .fractionalHeight(76))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        item.contentInsets = NSDirectionalEdgeInsets.init(top: 0, leading: 0, bottom: 8, trailing: 0)

        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                               heightDimension: .estimated(1))
        let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize, subitems: [item])
        let section = NSCollectionLayoutSection(group: group)
        section.contentInsets = NSDirectionalEdgeInsets.init(top: 12, leading: 20, bottom: 0, trailing: 20)

        return section
    }
}
