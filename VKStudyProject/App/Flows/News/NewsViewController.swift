import UIKit

class NewsViewController: UIViewController {

    // MARK: - Dependencyry()
    private let newsService = NetworkServiceFactory.shared.makeNewsRequestFactory()
    private var cellLayoutCalculator: FeedCellLayoutCalculatorProtocol = FeedCellLayoutCalculator()

    // MARK: - Outlets
    var tableView: UITableView!
    private var activityIndicator: UIActivityIndicatorView = {
        let aiv = UIActivityIndicatorView(style: .large)
        aiv.color = .black
        aiv.startAnimating()
        aiv.hidesWhenStopped = true
        return aiv
    }()
    let dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_Ru")
        dateFormatter.dateFormat = "d MMM 'в' HH:mm"
        return dateFormatter
    }()

    // MARK: - Properties
    var feedViewModel = FeedViewModel.init(cell: [])
    private var nextFrom = ""

    // MARK: - Controller lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableViewAndUI()
        fetchNewsFeed()
    }

    private func setupTableViewAndUI() {
        tableView = UITableView(frame: .zero, style: .plain)
        view.addSubview(tableView)
        tableView.fillSuperview()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = .lightGray
        tableView.allowsSelection = false

        tableView.register(NewsFeedCodeCell.self, forCellReuseIdentifier: NewsFeedCodeCell.cellId)
    }

    private func fetchNewsFeed() {

        newsService.getNews { (newsResponse) in
            guard let newsResponse = newsResponse else { return }

            self.nextFrom = newsResponse.response.nextFrom ?? ""

            let cells = newsResponse.response.items.map({ feedItem in
                self.cellViewModel(from: feedItem,
                                   profiles: newsResponse.response.profiles,
                                   groups: newsResponse.response.groups)
            })

            self.feedViewModel = FeedViewModel.init(cell: cells)

            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }

    private func cellViewModel(from feedItem: NewsFeedModel,
                               profiles: [ProfileNews],
                               groups: [GroupNews]) -> FeedViewModel.Cell {

        let profile = self.profile(for: feedItem.sourceId, profiles: profiles, groups: groups)
        let photoAttachments = self.photoAttachments(feedItem: feedItem)
        let date = Date(timeIntervalSince1970: feedItem.date)
        let dateTitle = dateFormatter.string(from: date)
        let postText = feedItem.text?.replacingOccurrences(of: "<br>", with: "\n")

        let sizes = cellLayoutCalculator.sizes(postText: feedItem.text, photoAttachmants: photoAttachments)

        return FeedViewModel.Cell.init(iconUrlString: profile.photo,
                                       name: profile.name,
                                       date: dateTitle,
                                       text: postText,
                                       likes: formattedCounter(feedItem.likes?.count),
                                       comments: formattedCounter(feedItem.comments?.count),
                                       shares: formattedCounter(feedItem.reposts?.count),
                                       views: formattedCounter(feedItem.views?.count),
                                       photoAttachements: photoAttachments,
                                       sizes: sizes)
    }

    private func profile(for sourceId: Int, profiles: [ProfileNews], groups: [GroupNews]) -> ProfileRepresentable {

        let profilesOrGroups: [ProfileRepresentable] = sourceId >= 0 ? profiles : groups
        let normalSourceId = sourceId >= 0 ? sourceId : -sourceId
        let profileRepresentable = profilesOrGroups.first { (myProfileRepresantable) -> Bool in
            myProfileRepresantable.idNumber == normalSourceId
        }
        return profileRepresentable!
    }

    private func photoAttachments(feedItem: NewsFeedModel) -> [FeedViewModel.FeedCellPhotoAttachment] {
        guard let attachments = feedItem.attachments else { return [] }
        return attachments.compactMap({ (attachment) -> FeedViewModel.FeedCellPhotoAttachment? in
            guard let photo = attachment.photo else { return nil }
            return FeedViewModel.FeedCellPhotoAttachment.init(
                photoUrlString: photo.srcBIG,
                width: photo.width,
                height: photo.height)
        })
    }

    private func formattedCounter(_ counter: Int?) -> String? {
        guard let counter = counter, counter > 0 else { return "" }
        var counterString = String(counter)

        if 4...6 ~= counterString.count {
            counterString = String(counterString.dropLast(3)) + "K"
        } else if counterString.count > 6 {
            counterString = String(counterString.dropLast(6)) + "M"
        }
        return counterString
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource

extension NewsViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedViewModel.cell.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: NewsFeedCodeCell.cellId,
            for: indexPath) as? NewsFeedCodeCell

        let cellViewModel = feedViewModel.cell[indexPath.row]
        cell?.set(viewModel: cellViewModel)

        return cell ?? UITableViewCell()
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellViewModel = feedViewModel.cell[indexPath.row]
        return cellViewModel.sizes.totalHeight
    }
}
