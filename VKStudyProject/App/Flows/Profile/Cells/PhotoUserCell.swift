import UIKit
import SDWebImage

class PhotoUserCell: UICollectionViewCell {

    static let reuserId = "PhotoUserCell"

    let imageView = UIImageView()

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupUI()
        backgroundColor = .yellow
    }

    private func setupUI() {
        addSubview(imageView)
        imageView.fillSuperview()
        imageView.layer.cornerRadius = 5
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
    }

    func setupCell(photoUser: PhotoUserItem?) {
        guard let imageString = photoUser?.srcBig else {
            imageView.image = #imageLiteral(resourceName: "nophoto")
            return
        }
        let url = URL(string: imageString)
        imageView.sd_setImage(with: url, completed: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
