import UIKit

class ProfileFriendsCell: UICollectionViewCell {

    static let reuserId = "ProfileFriendsCell"
    let cornerRadiusIcon: CGFloat = 50

    let iconFriend: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()

    let fullNameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont(name: "avenir", size: 20)
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupUI()
    }

    private func setupUI() {
        let horizontalStackView = UIStackView(arrangedSubviews: [
            iconFriend, fullNameLabel
        ])
        addSubview(horizontalStackView)
        horizontalStackView.axis = .horizontal
        horizontalStackView.spacing = 10
        horizontalStackView.fillSuperview(padding: .init(top: 5,
                                                         left: 0,
                                                         bottom: 5,
                                                         right: 0))

        horizontalStackView.layoutIfNeeded()
        let height = horizontalStackView.frame.height
        iconFriend.widthAnchor.constraint(equalToConstant: height).isActive = true
        iconFriend.layer.cornerRadius = height / 2
        iconFriend.layer.masksToBounds = true
    }

    func setupCell(friend: Profile?) {
        fullNameLabel.text = "\(friend?.firstName ?? "") \(friend?.lastName ?? "")"
        guard let iconString = friend?.photUserUrl else {
            iconFriend.image = #imageLiteral(resourceName: "nophoto")
            return
        }
        let url = URL(string: iconString)
        iconFriend.sd_setImage(with: url, completed: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
