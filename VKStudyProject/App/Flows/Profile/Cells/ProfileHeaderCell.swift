import UIKit

class ProfileHeaderCell: UICollectionViewCell {

    static let reuserId = "ProfileHeaderCell"

    let fullNameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 30)
        return label
    }()

    let homeTownLabel: UILabel = {
        let label = UILabel()
        return label
    }()

    let birthdayDateLabel: UILabel = {
        let label = UILabel()
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupUI()
    }

    private func setupUI() {
        let verticalStackView = UIStackView(arrangedSubviews: [
            fullNameLabel, homeTownLabel
        ])
        verticalStackView.axis = .vertical
        verticalStackView.spacing = 5
        let horizontalStackView = UIStackView(arrangedSubviews: [
            verticalStackView, UIView(), birthdayDateLabel
        ])
        horizontalStackView.spacing = 10
        horizontalStackView.alignment = .center
        addSubview(horizontalStackView)
        horizontalStackView.fillSuperview()
    }

    func setupCell(profile: ProfileInfoResponse?) {
        fullNameLabel.text = "\(profile?.firstName ?? "") \(profile?.lastName ?? "")"
        homeTownLabel.text = profile?.homeTown
        if birthdayDateLabel.text == "0.0.0" {
            birthdayDateLabel.text = ""
        }
        birthdayDateLabel.text = profile?.birthdayDate
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
