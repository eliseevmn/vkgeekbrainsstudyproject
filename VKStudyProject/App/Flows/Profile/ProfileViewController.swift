import UIKit

enum Section: CaseIterable {
    case header
    case friends
    case photo
}

enum GroupType: Hashable {
    case profile([ProfileResponse])
    case friends(Profile)
    case photo(PhotoUserItem)
}

class ProfileViewController: UIViewController {

    // MARK: - Dependency
    let profileService = NetworkServiceFactory.shared.makeProfilRequesFactory()
    let photoService = NetworkServiceFactory.shared.makePhotoRequestFactory()
    let friendsService = NetworkServiceFactory.shared.makeFriendsListRequestFactory()
    let groupsService = NetworkServiceFactory.shared.makeGroupsRequestFactory()
    var dataSource: UICollectionViewDiffableDataSource<Section, GroupType>?
    var profileResponse = [ProfileResponse]()
    var friendProfile: Profile?
    var photoProfile: PhotoUserItem?

    // MARK: - Outlets
    var collectionView: UICollectionView!

    // MARK: - Controller lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupCollectionView()
        createDataSource()
        fetchingData()
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    // MARK: - Private functions
    private func fetchingData() {
        profileService.getProfileInfo { (profileResponse) in
            self.friendsService.getFriends { (friendsResponse) in
                self.photoService.getPhotoUser { (photosResponse) in

                    guard let profileResponse = profileResponse else { return }
                    self.profileResponse.append(profileResponse)

                    var snapshot = NSDiffableDataSourceSnapshot<Section, GroupType>()
                    snapshot.appendSections([.header, .photo, .friends])
                    snapshot.appendItems([.profile([profileResponse])], toSection: .header)

                    friendsResponse?.response.items.forEach({ (profile) in
                        snapshot.appendItems([.friends(profile)], toSection: .friends)

                    })
                    photosResponse.forEach { (photo) in
                        snapshot.appendItems([.photo(photo)], toSection: .photo)
                    }
                    self.dataSource?.apply(snapshot)
                }
            }
        }
    }

    private func setupCollectionView() {
        collectionView = UICollectionView(frame: view.bounds, collectionViewLayout: createCompositionalLayout())
        collectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        collectionView.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.9725490196, blue: 0.9921568627, alpha: 1)
        view.addSubview(collectionView)

        collectionView.register(ProfileHeaderCell.self, forCellWithReuseIdentifier: ProfileHeaderCell.reuserId)
        collectionView.register(ProfileFriendsCell.self, forCellWithReuseIdentifier: ProfileFriendsCell.reuserId)
        collectionView.register(PhotoUserCell.self, forCellWithReuseIdentifier: PhotoUserCell.reuserId)
    }

    // MARK: - Manage the data in UI COmpositional layout
    func createDataSource() {
        dataSource = UICollectionViewDiffableDataSource<Section, GroupType>(
            collectionView: collectionView,
            cellProvider: { (collectionView, indexPath, groupType) -> UICollectionViewCell? in

            switch groupType {
            case .profile(let profile):
                let cell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: ProfileHeaderCell.reuserId,
                    for: indexPath) as? ProfileHeaderCell
                let profile = profile[indexPath.item]
                cell?.setupCell(profile: profile.response)
                return cell
            case .friends(let friend):
                let cell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: ProfileFriendsCell.reuserId,
                    for: indexPath) as? ProfileFriendsCell
                cell?.setupCell(friend: friend)
                return cell
            case .photo(let photo):
                let cell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: PhotoUserCell.reuserId,
                    for: indexPath) as? PhotoUserCell
                cell?.setupCell(photoUser: photo)
                return cell
            }
        })
    }

    private func createCompositionalLayout() -> UICollectionViewLayout {
        let layout = UICollectionViewCompositionalLayout { (sectionIndex, _) -> NSCollectionLayoutSection? in
            let section = Section.allCases[sectionIndex]
            switch section {
            case .header:
                return self.createProfileSection()
            case .friends:
                return self.createFriendsSection()
            case .photo:
                return self.createProfileSection()
            }
        }
        return layout
    }

    func createProfileSection() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                              heightDimension: .fractionalHeight(86))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        item.contentInsets = NSDirectionalEdgeInsets.init(top: 0, leading: 0, bottom: 8, trailing: 0)

        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                               heightDimension: .estimated(1))
        let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize, subitems: [item])
        let section = NSCollectionLayoutSection(group: group)
        section.contentInsets = NSDirectionalEdgeInsets.init(top: 12, leading: 20, bottom: 0, trailing: 20)

        return section
    }

    func createFriendsSection() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
                                              heightDimension: .fractionalHeight(1))
        let layoutItem = NSCollectionLayoutItem(layoutSize: itemSize)
        layoutItem.contentInsets = NSDirectionalEdgeInsets.init(top: 0, leading: 8, bottom: 0, trailing: 8)

        let layoutGroupSize = NSCollectionLayoutSize(widthDimension: .estimated(104),
                                                     heightDimension: .estimated(88))
        let layoutGroup = NSCollectionLayoutGroup.horizontal(layoutSize: layoutGroupSize, subitems: [layoutItem])

        let layoutSection = NSCollectionLayoutSection(group: layoutGroup)
        layoutSection.orthogonalScrollingBehavior = .continuous
        layoutSection.contentInsets = NSDirectionalEdgeInsets.init(top: 12, leading: 12, bottom: 0, trailing: 12)

        return layoutSection
    }
}
