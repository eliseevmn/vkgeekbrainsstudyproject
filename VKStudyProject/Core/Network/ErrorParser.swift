import Foundation

enum AppError: Error {
    case serverError
    case clientError
    case failData
    case unknownError
}

/// Получение ошибок
protocol AbstractErrorParser {

    /// Извлечение ошибок
    /// - Parameter result: извлекаемая ошибка
    func parse(_ result: Error) -> Error

    /// Извлечение сетевых ошибок
    /// - Parameters:
    ///   - response: ответ на сетевой запрос
    ///   - data: полученные данные
    ///   - error: полученная ошибка
    func parse(response: HTTPURLResponse?, data: Data?, error: Error?) -> Error?
}

class ErrorParser: AbstractErrorParser {
    func parse(_ result: Error) -> Error {
        return result
    }

    func parse(response: HTTPURLResponse?, data: Data?, error: Error?) -> Error? {
        return error
    }
}
