import UIKit
import Alamofire

/// Протокол для работы с сетевыми запросами
protocol NetworkService {

    /// Сетевый запрос
    func request<T: Decodable>(_ request: URLRequestConvertible,
                               completion: @escaping (T?) -> Void)
}

class NetworkServiceImpl: NetworkService {

    private let errorParcer: ErrorParser
    private let sessionManager: Session

    init(errorParser: ErrorParser, sessionManager: Session) {
        self.errorParcer = errorParser
        self.sessionManager = sessionManager
    }

    func request<T: Decodable>(_ request: URLRequestConvertible,
                               completion: @escaping (T?) -> Void) {
        sessionManager.request(request).validate().responseData { response in
            do {
                guard let data = response.data else {
                    completion(nil)
                    return
                }
                let value = try JSONDecoder().decode(T.self, from: data)
                completion(value)
            } catch {
                completion(nil)
            }
        }
    }
}
