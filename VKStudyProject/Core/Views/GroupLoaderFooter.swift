import UIKit

class MusicLoadingFooter: UICollectionReusableView {

    static let footerId = "MusicLoadingFooter"

    override init(frame: CGRect) {
        super.init(frame: frame)

        let aiv = UIActivityIndicatorView(style: .large)
        aiv.color = .darkGray
        aiv.startAnimating()

        let label = UILabel(text: "Loading more...", font: .systemFont(ofSize: 16))
        label.textAlignment = .center

        let stackView = UIStackView(arrangedSubviews: [
            aiv, label
            ], customSpacing: 8)
        addSubview(stackView)
        stackView.centerInSuperview(size: .init(width: 200, height: 0))
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
